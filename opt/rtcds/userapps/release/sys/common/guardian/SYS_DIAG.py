"""SYS DIAGNOSTIC GUARDIAN MODULE

*************************************
***** DO NOT MODIFY THIS MODULE *****
*************************************

This is the control code for Guardian system diagnostic nodes.  It is
meant to be loaded by system diagnostic node modules.

Tests are simple functions that yield diagnostic message srings.  They
are registered using the @SYSDIAG.register decorator:

@SYSDIAG.register_test
def TEST_NAME():
    if test_logic:
        yield MESSAGE

All registered tests are run (in random order) in the RUN_TESTS state.
A list of all registered tests can be printed to the node log by
requesting the INIT state (after which the system will return
automatically to RUN_TESTS).

If a test yields a string (MESSAGE), that string is raised as a
guardian "notification" and node's NOTIFICATION channel will become
True.  All notifications from all tests will show up in node USERMSG*
channels, with the form:

  TEST_NAME: MESSAGE

The RUN_TESTS status will be DONE, and the node status will be
OK=True, if no tests yield messages.  If they do, the RUN_TESTS status
will drop to RUN and the node status will be OK=False.

For help or questions contact:

Jameson Graef Rollins <jrollins@ligo.caltech.edu>

*************************************
***** DO NOT MODIFY THIS MODULE *****
*************************************

"""
##################################################

import functools

from guardian import GuardState

##################################################

class SysDiag(object):

    # dictionary of test (name, func) pairs.
    tests = {}

    # dictionary of test (name, kwargs) pairs.
    args = {}

    # log which test is running
    log = False

    def register_test(self, f):
        """Decorator for registering diagnostic tests.

        Wrap the diagnostic test as so:

        @sysdiag.register
        def FOO():
            ...

        The functions may define keyword arguments that are filled in
        from SysDiag.args at exectution time.

        """
        name = f.__name__
        #print "register: %s" % name
        self.tests[name] = f

        @functools.wraps(f)
        def decorator(*args, **kwargs):
            return f(*args, **kwargs)
        return decorator

    # DEPRECATE
    register = register_test

    def __len__(self):
        return len(self.tests)

    def __getitem__(self, name):
        """Get function of test by name.

        """
        return self.tests[name]

    def keys(self):
        """Return list of test names.

        """
        return self.tests.keys()

    def __iter__(self):
        """Iterator of test (name, func) pairs.

        """
        for pair in self.tests.items():
            yield pair

    def print_tests(self):
        """Write list of tests to log.

        """
        if len(self) == 0:
            log("No tests registered.")
            return
        log("Registered tests:")
        for name, func in self:
            doc = func.__doc__
            if doc:
                doc = doc.strip().split('\n')[0]
            else:
                doc = ''
            log("  %s: %s" % (name, doc))

    def run(self, name):
        """Run single test by name.

        Keyword arguments will be supplied from SysDiag.args if
        available.

        """
        kwargs = {}
        ret = True
        if name in self.args:
            kwargs = self.args[name]
        for msg in self[name](**kwargs):
            notify("%s: %s" % (name, msg))
            ret &= False
        return ret

    def run_all(self):
        """Run all tests.

        See SysDiag.run() for more info.

        """
        ret = True
        for name in self.keys():
            if self.log:
                log("test: %s" % name)
            ret &= self.run(name)
        return ret

SYSDIAG = SysDiag()

##################################################

def static_vars(**kwargs):
    def decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func
    return decorate

##################################################

request = 'RUN_TESTS'
nominal = 'RUN_TESTS'

class INIT(GuardState):
    def main(self):
        SYSDIAG.print_tests()
        return 'RUN_TESTS'

class RUN_TESTS(GuardState):
    index = 10
    def run(self):
        return SYSDIAG.run_all()

edges = [
    ('INIT', 'RUN_TESTS'),
    ]
